import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
	return(
		<Row className='mt-3 mb3'>

		<Col xs={12} md={4}>
		<Card className="cardHiglight p-3">
			      {/*<Card.Img variant="top" src="holder.js/100px180" />*/}
			      <Card.Body>
			        <Card.Title>Sample1</Card.Title>
			        <Card.Text>
			          Some quick example text to build on the card title and make up the
			          bulk of the card's content.
			        </Card.Text>
			        {/*<Button variant="primary">Go somewhere</Button>*/}
			      </Card.Body>
		</Card>
		</Col>

		<Col>
		<Card className="cardHiglight p-3">
			      {/*<Card.Img variant="top" src="holder.js/100px180" />*/}
			      <Card.Body>
			        <Card.Title>Sample2</Card.Title>
			        <Card.Text>
			          Some quick example text to build on the card title and make up the
			          bulk of the card's content.
			        </Card.Text>
			        {/*<Button variant="primary">Go somewhere</Button>*/}
			      </Card.Body>
		</Card>
		</Col>

		<Col>
		<Card className="cardHiglight p-3">
			      {/*<Card.Img variant="top" src="holder.js/100px180" />*/}
			      <Card.Body>
			        <Card.Title>Sample3</Card.Title>
			        <Card.Text>
			          Some quick example text to build on the card title and make up the
			          bulk of the card's content.
			        </Card.Text>
			        {/*<Button variant="primary">Go somewhere</Button>*/}
			      </Card.Body>
		</Card>
		</Col>
		
		</Row>
		)
}

	


