import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductsCard({productsProp}){
	// console.log(props)

	// const [count, setCount] = useState(100)

	let { name, description, price, _id } = productsProp;

	// let num = 0

	// function order(){
	// 	setCount(count - 1)
	// 	console.log('Stocks Available: ' + count)
	// }

	return (
		<Card className="mt-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>
				{/*<Card.Text>Stocks Available: {count}</Card.Text>*/}
				<Button as={Link} to={`/products/${_id}`}>Add to Cart</Button>
			</Card.Body>
		</Card>
		)
}
