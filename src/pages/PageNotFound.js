import Banner from '../components/Banner';

export default function PageNotFound(){
    
    const data  = {

        title: "404 - Page Not Found",
        content: "The page you are looking for cannot be found.",
        destination: "/",
        label: "Back Home"
    }

    return (
        // "props" name is up to the developer but we need to 
        <Banner bannerProp = {data}/>
        )

}