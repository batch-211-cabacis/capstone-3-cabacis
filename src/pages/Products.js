// import productsData from '../data/products';
import ProductsCard from '../components/ProductsCard';
import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';


export default function Products(){

	const [ products, setProducts ] = useState([])

	useEffect(()=>{
		fetch('http://localhost:4000/products/all')
		.then(res=>res.json())
		.then(data=>{


		const productsArr = (data.map(products =>{
		return (
			<ProductsCard productsProp={products} key={products._id}/>
			)
	}))
		setProducts(productsArr)
	})


	},[products])
	

	
	return(
		<>
		{products}
		</>


		)
}