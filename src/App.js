// import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar'
// import ProductsCard from './components/ProductsCard';
import Products from './pages/Products';
import ProductsView from './components/ProductsView';
import Home from './pages/Home';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Login from './pages/Login';
import PageNotFound from './pages/PageNotFound';
import './App.css';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';



function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = ()=>{
    localStorage.clear()
  }

  useEffect(()=>{
    fetch('http://localhost:4000/users/details',{
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{
      console.log(data);

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      }else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  },[]);

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
          <Container>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/logout" element={<Logout/>}/>
              <Route path="/products" element={<Products/>}/>
              <Route path="/products/:productsId" element={<ProductsView/>}/>
              <Route path="*" element={<PageNotFound/>}/>
            </Routes>
          </Container>
      </Router>
    </UserProvider>
    
    )
  }
    

export default App;


